package br.edu.cruzeirodosul.webapps.devutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.SelectableChannel;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected String takeLogDir() {
		String logDir = System.getProperty("jboss.server.log.dir");
		if(logDir==null) throw new RuntimeException("Propriedade do caminho do log nao foi encontrado");
		return logDir;
	}
	
	protected void showAllProperties(OutputStream out) throws IOException {
		Properties pop = System.getProperties();
		Set<Object> keys = pop.keySet();
		out.write("----------------------------------------------------------------------------\n".getBytes());
		for(Object key : keys) {
			String value = pop.get(key).toString();
			out.write(key.toString().getBytes());
			out.write("=".getBytes());
			out.write(value.getBytes());
			out.write("\n".getBytes());
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sLeftByte = request.getParameter("leftByte");
		Long leftByte;
		try {
			leftByte = Long.valueOf(sLeftByte);
		}catch(Exception e) {
			leftByte=1000l;
		}
		
		
		response.setContentType("text/plain; name=\"server.log\"");
		response.setHeader("Content-Disposition", "inline; filename=\"server.log\"");
		
		
		String caminho=takeLogDir()+"\\";
		File log = new File(caminho + "server.log");
		
		long seekPoint = log.length() - leftByte;
		if(seekPoint<0) seekPoint=0;
		
		FileInputStream in = new FileInputStream(log);
		in.skip(seekPoint);
		
		byte[] buf = new byte[100];
		int readed;
		OutputStream out = response.getOutputStream();
		while((readed=in.read(buf))!=-1) {
			out.write(buf, 0, readed);
		}
		
		
		
		
		
		
		
		out.close();
		in.close();
	}
}
